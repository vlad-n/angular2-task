import {Component} from 'angular2/core';
import {Task} from './task';
import {stateFilter} from './filters/stateFilter';
import {searchText} from './filters/searchText';
import {propertyFilter} from './filters/propertyFilter';
@Component({
    templateUrl: "./templates/task.tmpl.html",
    pipes: [stateFilter, searchText, propertyFilter]

})
export class TaskComponent {
    public tasks = TASKS;
    public stateTasks = stateTasks;
    public checkBoxState = checkBoxState;
    public stCount = stateCount;

    changeState(stateValue) {
        this.stateTasks = stateValue;
    }
}
var stateTasks:string = "in";

var checkBoxState = {personal: false, night: false, urgent: false};
var stateCount = {
    in: 0,
    new: 0,
    underConsideration: 0,
    done: 0,
    canceled: 0,
    draft: 0
};

var TASKS:Task[] = [{
    id: "001",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    data: "14 декабря",
    sum: "10 000",
    state: "in",
    personal: "false",
    night: "true",
    urgent: "false"
}, {
    id: "002",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    data: "14 декабря",
    state: "in",
    personal: "true",
    night: "false",
    urgent: "true"
}, {
    id: "003",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    data: "14 декабря",
    state: "in",
    personal: "false",
    night: "false",
    urgent: "true"
}, {
    id: "004",
    city: {id: "001", name: "Самара"},
    data: "14 декабря",
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "in",
    personal: "true",
    night: "true",
    urgent: "false"
}, {
    id: "005",
    city: {id: "001", name: "Самара"},
    data: "14 декабря",
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    personal: "true",
    state: "new",
    personal: "false",
    night: "true",
    urgent: "false"
}, {
    id: "006",
    city: {id: "001", name: "Самара"},
    data: "14 декабря",
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "new",
    personal: "true",
    night: "false",
    urgent: "true"
}, {
    id: "007",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    data: "14 декабря",
    sum: "10 000",
    data: "14 декабря",
    state: "new",
    personal: "true",
    night: "true",
    urgent: "true"
}, {
    id: "008",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "new",
    personal: "true",
    data: "14 декабря",
    night: "false",
    urgent: "false"
}, {
    id: "009",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "in",
    personal: "false",
    data: "14 декабря",
    night: "true",
    urgent: "true"
}, {
    id: "0010",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "underConsideration",
    personal: "false",
    data: "14 декабря",
    night: "false",
    urgent: "false"
}, {
    id: "0011",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "underConsideration",
    personal: "true",
    data: "14 декабря",
    night: "false",
    urgent: "false"
}, {
    id: "0012",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "underConsideration",
    personal: "false",
    data: "14 декабря",
    night: "true",
    urgent: "false"
}, {
    id: "0013",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "underConsideration",
    data: "14 декабря",
    personal: "true",
    night: "true",
    urgent: "true"
}, {
    id: "0014",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "done",
    personal: "true",
    data: "14 декабря",
    night: "false",
    urgent: "true"
}, {
    id: "0015",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "done",
    personal: "true",
    data: "14 декабря",
    night: "true",
    urgent: "false"
}, {
    id: "0016",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "done",
    personal: "false",
    data: "14 декабря",
    night: "true",
    urgent: "false"
}, {
    id: "0017",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "done",
    personal: "false",
    data: "14 декабря",
    night: "false",
    urgent: "false"
}, {
    id: "0018",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "done",
    personal: "true",
    data: "14 декабря",
    night: "false",
    urgent: "false"
}, {
    id: "0019",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "canceled",
    personal: "false",
    data: "14 декабря",
    night: "false",
    urgent: "true"
}, {
    id: "0020",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "canceled",
    personal: "false",
    data: "14 декабря",
    night: "false",
    urgent: "false"
}, {
    id: "0021",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "canceled",
    data: "14 декабря",
    personal: "false",
    night: "true",
    urgent: "false"
}, {
    id: "0022",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "canceled",
    personal: "true",
    data: "14 декабря",
    night: "false",
    urgent: "true"
}, {
    id: "0023",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "canceled",
    personal: "true",
    data: "14 декабря",
    night: "true",
    urgent: "true"
}, {
    id: "0024",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "canceled",
    personal: "true",
    data: "14 декабря",
    night: "true",
    urgent: "true"
}, {
    id: "0025",
    city: {id: "001", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "draft",
    personal: "true",
    data: "14 декабря",
    night: "true",
    urgent: "true"
}, {
    id: "0025",
    city: {id: "A1", name: "Самара"},
    name: "Чистка фьюзера, смазка и прочистка подвижных механизмов и роликов, чистка внутренних механизмов от пыли и тонера..",
    data: "14 декабря",
    sum: "10 000",
    state: "in",
    personal: "true",
    night: "true",
    urgent: "true"
}, {
    id: "0026",
    city: {id: "A2", name: "Самара"},
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    data: "14 декабря",
    state: "new",
    personal: "true",
    night: "false",
    urgent: "true"
}, {
    id: "0027",
    city: {id: "A3", name: "Самара"},
    name: "Установка видеорегистратора, подключение камер, подключение к локальной сети, настройка.",
    sum: "10 000",
    data: "14 декабря",
    state: "done",
    personal: "false",
    night: "false",
    urgent: "true"
}, {
    id: "0028",
    city: {id: "A4", name: "Самара"},
    data: "14 декабря",
    name: "Чистка фьюзера, смазка и прочистка подвижных механизмов и роликов, чистка внутренних механизмов от пыли и тонера.",
    sum: "10 000",
    state: "canceled",
    personal: "true",
    night: "true",
    urgent: "false"
}, {
    id: "0029",
    city: {id: "A5", name: "Самара"},
    data: "14 декабря",
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    personal: "true",
    state: "draft",
    personal: "false",
    night: "true",
    urgent: "false"
}, {
    id: "0030",
    city: {id: "A6", name: "Самара"},
    data: "14 декабря",
    name: "Ремонт и замена термопленки, термоэлемента, фьюзера. Замена роликов захвата бумаги, подачи и переноса. Восстановление механизмов мотора.",
    sum: "10 000",
    state: "underConsideration",
    personal: "true",
    night: "false",
    urgent: "true"
}];

var propertyArray = Object.keys(stateCount);
var func = TASKS.forEach(function (task, i, arr) {
    propertyArray.forEach(function (prop, i, arr) {
        if (prop == task.state) {
            stateCount[prop]++
        }
        else {}
    });

});