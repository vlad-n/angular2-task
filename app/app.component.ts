import {Component} from 'angular2/core';

import {TaskComponent}     from './task.component';
import {CompanyComponent}     from './company.component';
import {FinanceComponent}     from './finance.component';
import {StatisticsComponent}     from './statistics.component';
import {NgClass} from 'angular2/common';
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';
@Component({
    selector: 'my-app',
    templateUrl: "./templates/app.tmpl.html",
    directives: [ROUTER_DIRECTIVES, NgClass]
})

@RouteConfig([
    {path: '/task', name: 'TaskComponent', component: TaskComponent},
    {path: '/company', name: 'CompanyComponent', component: CompanyComponent},
    {path: '/finance', name: 'FinanceComponent', component: FinanceComponent},
    {path: '/statistics', name: 'StatisticsComponent', component: StatisticsComponent}
])

export class AppComponent {
}

