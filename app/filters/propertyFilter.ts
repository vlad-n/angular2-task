import {Pipe, PipeTransform} from 'angular2/core';
import {implementsOnDestroy} from "angular2/src/core/change_detection/pipe_lifecycle_reflector";
@Pipe({
    name: 'propertyFilter'
})

export class propertyFilter {
    transform(items:any[], data:any[]):any {
        var obj = data[0];
        if (obj.personal == false && obj.urgent == false && obj.night == false) {
            return items
        }
        else {
            return items.filter(function (item) {
                var propertyArray = Object.keys(obj);
                var check = [];
                propertyArray.forEach(function (prop) {

                    if (obj[prop].toString() == item[prop]) {
                        check.push("true");
                    }
                    else {
                        check.push("false");
                    }
                });

                function checkTrue(element, index, array) {
                    return element == "true";
                };
                return check.every(checkTrue);
            });
        }

    }
}

