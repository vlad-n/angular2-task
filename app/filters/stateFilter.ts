import {Pipe, PipeTransform} from 'angular2/core';
import {implementsOnDestroy} from "angular2/src/core/change_detection/pipe_lifecycle_reflector";
@Pipe({
    name: 'stateFilter'
})

export class stateFilter {
    transform(items:any[], state:string):any {
        return items.filter(function (item) {
            return item.state == state;
        });
    }
}

