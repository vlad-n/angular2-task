import {Pipe, PipeTransform} from 'angular2/core';
import {implementsOnDestroy} from "angular2/src/core/change_detection/pipe_lifecycle_reflector";
@Pipe({
    name: 'searchText'
})

export class searchText {
    transform(items:any[], text:string):any {
        return items.filter(function (item) {
            var check = [];
            for (key in item) {
                if (typeof(item[key]) == "object") {
                    var obj = item[key];
                    for (key in obj) {
                        var res = obj[key].indexOf(text, 0);
                        if (res == -1) {
                            check.push("fasle");
                        }
                        else if (res !== -1) {
                            check.push("true");
                        }
                    }
                }

                var res = item[key].indexOf(text, 0);

                if (res == -1) {
                    check.push("fasle");
                }
                else if (res !== -1) {
                    check.push("true");
                }
            }

            function checkTrue(element, index, array) {
                return element == "true";
            };
            return check.some(checkTrue);
        });
    }
}

